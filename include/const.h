#pragma once
#include"Headers.h"
class constOp;
void displayVal(constOp* o);
class constOp {
	mutable int countOf = 0;
	const int a = 10;
	int b;

public:
	int y = 100;
	constOp():b(12){}
	explicit constOp(int c):b(c){}
	const int* const getValue() const {
		//a++;  //cannot be done
		countOf++; //possible cause of mutable
		//b++; //not possible as it is a const method
		const int *temp= new int();
		temp = (int*)&a;
		return temp;

	}
	void display() {
	
		const int* x = new int;
		//*x = 2;  //cannot be assigned  //pointer cannit be modified
		x = (int*)&a;
		cout << "const before int*" << *x;
		int* const xx = new int;
		*xx = 2;
		//xx = (int*)&a; //cannot be referenced contents cannot be modified
		cout << "const after int*" << *xx;

		displayVal(this);
		



	}
};

void displayVal(constOp* o) {
	cout << "<<<<<<" << o->y;
}



