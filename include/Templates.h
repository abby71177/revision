#pragma once
#include"Headers.h"

template <typename T>
class Stack {
	T* datas;
	int size;
	int top;
public:
	Stack() : size(10), top(-1) {
		datas = new T[size];
	}
	int isEmpty(){ return (top == -1 ? 1 : 0);}
	int isFull() { return (top == size ? 1 : 0); }
	void push( T );
	T pop();
	void display();

};
template <typename T>
void Stack<T> ::push(T data) {
	if (isFull()) {
		cout << "Stack full\n";
	}
	else {
		top++;
		*(datas+top) = data;
	}
 }

template<typename T>
T Stack<T>:: pop() {
	if (isEmpty()) {
		cout << "Stack empty\n";
	}
	else {
		top--;
		return *(datas+(top+1));
	}
}

template<typename T>
void Stack<T>:: display() {
	for (int i = top;i >= 0;i--)
		cout << *(datas+i) << endl;
}

void call() {
	Stack<int> st;
	st.push(10);
	st.push(20);
	st.push(30);
	st.push(40);
	st.push(50);
	st.display();
	cout << st.pop() << "is popped\n";
	st.display();

	Stack<string> s;
	s.push("AA");
	s.push("BB");
	s.push("CC");
	s.push("DD");
	s.push("EE");
	s.push("FF");
	s.display();
	cout << s.pop() << "is popped\n";
	s.display();


}