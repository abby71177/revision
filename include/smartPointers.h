#pragma once
#include"Headers.h"

class smartPointers {
	 const char* name;
public:
	smartPointers():name("Unknown") {
		cout << "\nConstructor";

	}
	smartPointers(const char* name_2):name(name_2) {
		cout << "\nConstructor";

	}
	smartPointers(smartPointers &sp)  {
		this->name = sp.name;
		cout << "\nConstructor";

	}

	~smartPointers() {
		cout << "\nDestructor";

	}

	void displayName() {
		cout << "\nPointer name  "<<this->name;
	}
};

void operation() {
	{
		unique_ptr< smartPointers> uniquePointer = make_unique< smartPointers>();
		uniquePointer->displayName();
	}
	unique_ptr< smartPointers> uniquePointer2 = make_unique< smartPointers>("unique2");
	smartPointers sp("unique3");
	unique_ptr< smartPointers> uniquePointer3 = make_unique< smartPointers>(sp);
	uniquePointer2->displayName();
	uniquePointer3->displayName();
	{
		shared_ptr<smartPointers> sharedPointer = make_shared<smartPointers>();
		shared_ptr<smartPointers> sharedPointer2 = make_shared<smartPointers>("shared2");

		sharedPointer = sharedPointer2;
		sharedPointer->displayName();
		weak_ptr<smartPointers> weakPointer = sharedPointer;

	}





}
