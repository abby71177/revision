#pragma once
#include<iostream>
#include<map>
#include<tuple>
#include<array>
#include<string>
using namespace std;
void arrayManu(array<int, 5> arr) {
	for (int i = 0;i < arr.size();i++) arr[i] *= 100;

}
void arrayOp() {
	array<int, 5> arr;
	for (int i = 0;i < 10;i++) arr[i] = i + 1;
	arrayManu(arr);
}

class student {
	string regNo;
	string name;
	string dept;
	int year;
public:
	student(string regNo, string name, string dept, int year) {
		this->dept = dept;
		this->name = name;
		this->regNo = regNo;
		this->year = year;
	}
	tuple<student *> returnData() {
		return make_tuple(this);

	}
	void display(student* s) {
		cout << s->name << s->regNo << s->dept << s->year << endl;
	}
	string returnName() {
		return this->name;
	}
};

class studentRecords 
{

	map <string, tuple<student*>> rec;
public:
	studentRecords() {
		unique_ptr<student> ptr = make_unique<student>("5007", "Abby", "IT", 4);
		rec.insert(pair<string, tuple<student*>>{"5007", ptr->returnData()});
		
		unique_ptr<student> ptr2 = make_unique<student>("5009", "Aish", "IT", 4);
		rec["5009"] = ptr2->returnData();
		unique_ptr<student> ptr3 = make_unique<student>("5047", "Jags", "IT", 4);
		rec["5047"] = ptr3->returnData();
		unique_ptr<student> ptr4 = make_unique<student>("5002", "Bob", "IT", 4);
		rec["5002"] = ptr4->returnData();
		map <string, tuple<student*>> ::iterator i;
		pair<string, string> st;


		for (i = rec.begin();i != rec.end();i++)
		{
			cout << i->first;
			tuple<student*> stud = i->second;
			ptr->display(get<0>(stud));
			st = { get<0>(stud)->returnName(),get<0>(stud)->returnName() };

		}
		cout << endl << endl << st.first << st.second;

	}

	auto insert(tuple<student*> st) {
		rec.insert(pair <string, tuple<student*>>{ get<0>(st)->returnName(), get<0>(st)});
		return rec[get<0>(st)->returnName()];

	}

};
