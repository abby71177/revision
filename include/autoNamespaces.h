#pragma once
#include "Headers.h"
#include<array>
#include "tuplesMap.h"

namespace autoKey{
	void display() {
		vector< string> arr = { "aby","baby","celine","david" };
		for (auto element : arr)
			cout << element << " ";

		cout << endl;
		for (auto i = arr.begin();i < arr.end();i++)
			cout << *i;

		cout << endl;
		array <int,5> arrA = { 12,20,30,40,50 };
		for (auto element : arrA) cout << element << " ";


	}
	void displayStudent(){
		student *s=new student("5011", "mike", "it", 4);
		studentRecords stt;
		auto op = stt.insert(make_tuple(s));
		s->display(get<0>(op));

	}
	namespace _namespaces {
		void displayStudent() {
			student* s = new student("5069", "Stuart", " IT ", 4);
			studentRecords stt;
			auto op = stt.insert(make_tuple(s));
			s->display(get<0>(op));
		}
	};


	};
