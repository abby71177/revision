#include<stack>
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

int maximum(stack< int> st) {
	int m_value = st.top();
	while (!st.empty()) {
		if (st.top() >= m_value) m_value = st.top();
		st.pop();
	}
	return m_value;
}
void displayMatrix(vector < vector<int>>& arr) {
	for (int i = 0;i < arr.size();i++) {
		cout << endl;
		for (int j = 0;j < arr[i].size();j++)
			cout << arr[i][j] << " ";
	}
}


void createVector() {
	vector<int> arr;
	vector<int> arr1(5, 10);
	vector<int> arr2 = arr;
	vector<int> arr3 = { 10,20,30,40 };
	int temp;
	for (int i = 0;i < 5;i++) {
		cout << endl << "Enter value ";
		cin >> temp;
		arr.push_back(temp);
	}
	sort(arr.begin(), arr.end());
	for (auto value : arr) {
		cout << value << " ";
	}
	cout << "\nEnter the values of 3 X 3 matrix ";
	char ch;
	vector< vector<int>> matrix;
	for (int i = 0;i < 3;i++) 
	{
		vector <int> buffer;
		for (int j = 0;j < 3;j++) 
		{
			cout << " enter value ";
			cin >> ch;
			buffer.push_back(int(ch) - 97);

		}
		matrix.push_back(buffer);

	}
	displayMatrix(matrix);
}
