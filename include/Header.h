#pragma once
#include "Headers.h"
class demo {
public:
	string empName;
	string empNo;
	demo(): empName("Null"), empNo("Null"){}
	demo(string name, string no):empName(name), empNo(no){}
	void display() {
		cout << this->empName << " " << this->empNo;
	}
};
struct stack {
	int data;
	struct stack* next;
};
class stackOp {
	struct stack* top;
public:
	stackOp(): top(nullptr){}
	void push(int d) {
		struct stack* newNode = (struct stack *)malloc(sizeof(struct stack));
		newNode->data = d;
		newNode->next = nullptr;
		if (top == nullptr) {
			top= newNode;
		}
		else {
			newNode->next =top;
			top = newNode;
		}
	}

	void pop() {
		if (top == nullptr) cout << "Stck is empty";
		else {
			cout << top->data << " is popped" << endl;
			top = top->next;
		}
	}

	void display() {
		struct stack* temp = top;
		while (temp != nullptr) {
			cout << temp->data << endl;
			temp = temp->next;
		}
	}
};

//enum classType
//{
//	business=2000,economic=5000
//};
enum classTypeCode:char
{
	business = 'B', economic = 'E'
};
void checkClass(classTypeCode type) {
	switch (type) {
	case business: cout << "buiness class"<<type; break;
	case economic: cout << "ecomominc class" << type; break;
	}
}

static int s_value = 10;
class entity {

	static int x, y;
public:
	entity() {
		cout << "Constructor";
	}
	~entity() {
		cout << "destructor";
	}
	void display() { cout <<endl<< this->x << "  " << this->y; }

};
int entity::x;
int entity::y;

class master {
	const string name;
public:
	master(): name ("master") { }
	
	virtual void display() {
		cout << this->name;
	}

	void display2(){ cout << ">>>>>>>"<<this->name<<endl; }
};
class slave:public master {
	const string name;
public:
	slave(): name("slave"){}
	void display() override {
		cout << this->name;
	}
	void display2() { cout <<">>>>>>>>>>>>>>>"<< this->name<<endl; }
};

class base {
public:
	virtual void getName() = 0;
};

class derieved :public base {
public:
	void getName() override {
		cout << "getname";
	}
};


