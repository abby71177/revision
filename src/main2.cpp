#include "vectorsStack.h"
#include "tuplesMap.h"
#include"Templates.h"
#include "autoNamespaces.h"
int main() {
	stack<int> s;
	s.push(100);
	s.push(200);
	s.push(300);
	s.push(845);
	s.push(78);
	s.push(451);
	s.push(79);
	s.push(50);
	s.push(10);
	cout << "Maximum element in the stack is " << maximum(s)<<endl;
	cout << "Elements of the stack are\n";

	while (!s.empty()) {
		cout << s.top() << endl;
		s.pop();
	}
	//createVector();
	studentRecords();
	call();
	autoKey::display();
	cout << endl << endl;
	autoKey::displayStudent();
	autoKey::_namespaces::displayStudent();
	using namespace autoKey::_namespaces;
	displayStudent();

	
}
